// MyListApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "assert.h"
#include "list.h"

int main(int argc, char* argv[])
{
	list list1;
	assert(list1.size()==0);
	assert(list1.empty()==true);
	list1.push_back(1);
	list1.push_back(2);
	list1.push_back(3);
	list1.push_front(4);
	list1.push_front(5);
    //should be 5 4 1 2 3
    assert(list1.size()==5);
	assert(list1.empty()==false);
	assert(list1.front()==5);
	assert(list1.back()==3);
	assert(*list1.begin()==5);
	assert(*list1.cbegin()==5);

    list_iterator iter1=list1.begin();
    for(; iter1!=list1.cend(); ++iter1)
	{
        std::cout << *iter1;
        std::cout << std::endl;
	}

	list list2(list1);
    list_iterator iter2=list2.begin();
    for(; iter2!=list2.cend(); ++iter2)
	{
        std::cout << *iter2;
        std::cout << std::endl;
	}


	list1.clear();
	assert(list1.empty()==true);
    assert(list1.size()==0);

	return 0;
}
