# Set a project name an version number for our library
project(mylist VERSION 1.0)

# List header files
set(HDRS
  list.h

)

# List source files
set(SRCS
  MyListApp.cpp
  list.cpp
)



# Add rules to create a library
add_library(${PROJECT_NAME} SHARED ${SRCS} ${HDRS})
#add_library(${PROJECT_NAME} STATIC ${SRCS} ${HDRS})

# Tell the compiler to use the our compile flags
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS ${MY_COMPILE_FLAGS})

