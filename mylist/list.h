// list.h: interface for the list class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LIST_H__82D22AD9_4EF3_4EA2_A2EF_4D3DD9F0EF20__INCLUDED_)
#define AFX_LIST_H__82D22AD9_4EF3_4EA2_A2EF_4D3DD9F0EF20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef int TYPE;

struct list_node
{
	TYPE data;
    list_node *prev, *next;
};

class list_iterator
{
	list_node *p;
public:
	void test();
	list_iterator(list_node *p);
	TYPE & operator*();
	operator list_node*();
	list_iterator  operator++();
	list_iterator  operator--();
};

class list_const_iterator:public list_iterator
{
public:
	list_const_iterator(list_node *p);
	~list_const_iterator();
	const TYPE& operator*();
	list_const_iterator  operator++();
	list_const_iterator  operator--();
};

//  this list has a addiontial tailNode, if list is empty, and only a tailNode exist, and size=0
//  ->tailNode
//  if a list has a element 1, size=1, it have two nodes, 
//  ->1->tailNode

class list  
{
private:
	list_node *head,*tail;
	int m_size;
public:
	int size();
	void clear();
	list();
	list(list& list1);
	virtual ~list();
	bool empty();
	TYPE back();
	TYPE front();
	list_iterator end();
	list_iterator begin();
	list_const_iterator cend();
	list_const_iterator cbegin();
	void push_front(TYPE data);
	void push_back(TYPE data);
};

#endif // !defined(AFX_LIST_H__82D22AD9_4EF3_4EA2_A2EF_4D3DD9F0EF20__INCLUDED_)
