// mylib
#include "mylibrary/functions.h"
#include "mylibrary/list.h"

// stl
#include <iostream>
#include <stdexcept>

#include <assert.h>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;

using namespace mylib;
using namespace std;


int main(int argc, char** argv) try {
  /*UNUSED(argc)
  UNUSED(argv)

  mylib::helloWorld( "... anywho ..." );


  mylib::List list{2,3,4};*/



    mylib::list list1;
    assert(list1.size()==0);
    assert(list1.empty()==true);
    list1.push_back(1);
    list1.push_back(2);
    list1.push_back(3);
    list1.push_front(4);
    list1.push_front(5);
    //should be 5 4 1 2 3
    assert(list1.size()==5);
    assert(list1.empty()==false);
    assert(list1.front()==5);
    assert(list1.back()==3);
    assert(*list1.begin()==5);
    assert(*list1.cbegin()==5);

    list_const_iterator iter1=list1.cbegin();
    for(; iter1!=list1.cend(); ++iter1)
    {
        cout << *iter1;
        cout << endl;
    }

    list list2(list1);
    list_const_iterator iter2=list2.cbegin();
    for(; iter2!=list2.cend(); ++iter2)
    {
        cout << *iter2;
        cout << endl;
    }


    list1.clear();
    assert(list1.empty()==true);
    assert(list1.size()==0);

    return 0;

  return 0;
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
