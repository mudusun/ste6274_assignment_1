
#include "list.h"







namespace mylib {



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

list::list()
{
    //tailNode is an addtional node, it is used to record the list tail;
   list_node *tailNode =new list_node;
   tailNode->data=0;
   tailNode->next=0;
   tailNode->prev=0;

   head=0;
   tail=tailNode;
   m_size=0;
}
list::list(list& list1)
{
    list_node *tailNode =new list_node;
    tailNode->data=0;
    tailNode->next=0;
    tailNode->prev=0;
    head=0;
    tail=tailNode;
    m_size=0;

    list_const_iterator it1=list1.cbegin();
    for(;it1!=list1.end(); ++it1)
    {
       this->push_back(*it1);
    }
}
list::~list()
{
   clear();
   delete tail;
}


void list::push_back(TYPE data)
{
    list_node *p = new list_node;
    p->data = data;

    list_node *last_node=tail->prev;

    if (last_node)
    {
        last_node->next = p;
    }
    else
    {
        head = p;
    }
    p->prev = last_node;
    p->next = tail;
    tail->prev=p;
    m_size++;
}

void list::push_front(TYPE data)
{
    list_node *p = new list_node;

    p->data = data;
    p->prev = 0;

    if (head)
    {
        p->next = head;
        head->prev = p;
    }
    else
    {
        tail->prev = p;
        p->next=tail;
    }
    head = p;
    m_size++;
}
TYPE list::front()
{
    return head->data;
}

TYPE list::back()
{
    if(tail->prev)
    {
      return tail->prev->data;
    }
    return 0;
}

bool list::empty()
{
    if(m_size==0)
    {
        return true;
    }

    return false;
}
int list::size()
{
    return m_size;
}
void list::clear()
{
    list_node *p,*p1;
    p =p1 =head;
    while(p&&p!=tail)
    {
       p1=p->next;
       delete (p);
       p=p1;
    }
    head=0;
    m_size=0;
}
list_iterator list::begin()
{
   return head;
}

list_iterator list::end()
{
   return tail;
}

list_const_iterator list::cbegin()
{
    return head;
}

list_const_iterator list::cend()
{
    return tail;
}


void list_iterator::test()
{

}


list_iterator::list_iterator(list_node *p)
{
    this->p= p;
}

TYPE & list_iterator::operator*()
{
    return p->data;
}

list_iterator::operator list_node*()
{
    return p;
}

list_iterator  list_iterator::operator++()
{
    p= p->next;
    return *this;
}

list_iterator list_iterator::operator--()
{
    p = p->prev;
    return *this;
}

const TYPE& list_const_iterator::operator*()
{
    return list_iterator::operator*();
}
list_const_iterator  list_const_iterator::operator++()
{
    // increment const first!

    return *this;
}

list_const_iterator list_const_iterator::operator--()
{
    // remember to decrement first!

    return *this;
}

list_const_iterator::list_const_iterator(list_node* p):list_iterator(p)
{

}
list_const_iterator::~list_const_iterator()
{

}



} // END namespace mylib


