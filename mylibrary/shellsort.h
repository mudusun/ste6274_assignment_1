#ifndef __SHELLSORT_H__
#define __SHELLSORT_H__

#include <functional>
#include <vector>
#include <iterator>
#include <algorithm>
#include <type_traits>
#include <iostream>

namespace mylib {


  // Compiler/linker automatically deduces the typename of RandomAccessIterator into a std::iterator_traits variant
  // template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>


  template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
  void shellSort ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() ) {

    using size_type = typename RandomAccessIterator::difference_type;   // Deduce size_type from Random Access Iterator
    const size_type n = last-first;
    
    for( size_type gap {n/2}; 0 < gap; gap /= 2 ) 
      for( size_type i {gap}; i < n; i++ )
        for( size_type j {i-gap}; 0 <= j; j -= gap )
          if( cmp(first[j+gap],first[j]))
            std::swap(first[j],first[j+gap]); 
  }
  
} // END namespace mylib

#endif // __SHELLSORT_H__

